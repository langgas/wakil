<?php
/*
 * (c) 2014 Sisdik, <info@sisdik.com>
*/

define('MODE_DEBUG', 0);
define('JADWAL_DIR', 'jadwal' . DIRECTORY_SEPARATOR);
define('BERKAS_KEHADIRAN', 'kehadiran.json');
define('BERKAS_KEPULANGAN', 'kepulangan.json');
define('INTERVAL_WAKTU', 180); // 3 menit; untuk menjalankan cron; belum digunakan
define('BEDA_WAKTU_MAKS', 910); // 15 menit + 10 detik
define('BEDA_WAKTU_ATUR_MAKS', 900); // 15 menit
define('BEDA_WAKTU_PULANG_MAKS', 910); // 15 menit + 10 detik
define('KONEKSI_FP_TIMEOUT', 30);
define('LOG_DIR', 'log' . DIRECTORY_SEPARATOR);
define('LOG_DIR_PULANG', 'pulang' . DIRECTORY_SEPARATOR);
define('LOG_DIR_HARIAN_REMOTE', '/log' . DIRECTORY_SEPARATOR . 'a-harian' . DIRECTORY_SEPARATOR);
define('LOG_DIR_MINGGUAN_REMOTE', '/log' . DIRECTORY_SEPARATOR . 'b-mingguan' . DIRECTORY_SEPARATOR);
define('LOG_DIR_BULANAN_REMOTE', '/log' . DIRECTORY_SEPARATOR . 'c-bulanan' . DIRECTORY_SEPARATOR);
define('LOG_DIR_MANUAL_REMOTE', '/log' . DIRECTORY_SEPARATOR . 'manual' . DIRECTORY_SEPARATOR);
define('LOG_HARIAN', LOG_DIR . 'harian.log');
define('LOG_MINGGUAN', LOG_DIR . 'mingguan.log');
define('LOG_BULANAN', LOG_DIR . 'bulanan.log');
define('LOG_MANUAL', LOG_DIR . 'manual.log');
define('LOG_TERTIB_HARIAN', LOG_DIR . 'tertib-harian.log');
define('LOG_TERTIB_MANUAL', LOG_DIR . 'tertib-manual.log');
define('LOG_USER_KEHADIRAN', LOG_DIR . 'user-kehadiran.log');
define('DATA_DIR', 'data' . DIRECTORY_SEPARATOR);
define('FTP_SERVER', 'www.sisdik.com');
define('FTP_USERNAME', 'ftpusername');
define('FTP_PASSWORD', 'ftppassword');
define('API_TOKEN', 'apitoken');
define('SMS_PROVIDER', "https://provider.com/smsapi.php?nohp=%nomor%&pesan=%pesan%");
define('NON_SSL', false);
define('DEV_BOX', '');
