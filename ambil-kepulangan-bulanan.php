<?php
/*
 * (c) 2014 Sisdik, <info@sisdik.com>
 */

require_once 'konfigurasi.php';

require_once 'lib/TADFactory.php';
require_once 'lib/TAD.php';
require_once 'lib/TADResponse.php';
require_once 'lib/Providers/TADSoap.php';
require_once 'lib/Providers/TADZKLib.php';
require_once 'lib/Exceptions/ConnectionError.php';
require_once 'lib/Exceptions/FilterArgumentError.php';
require_once 'lib/Exceptions/UnrecognizedArgument.php';
require_once 'lib/Exceptions/UnrecognizedCommand.php';

$jsonString = file_get_contents(JADWAL_DIR . BERKAS_KEPULANGAN);
$rawJadwal = json_decode($jsonString, true);
$apiJadwal = array();

if (array_key_exists('a-harian', $rawJadwal['jadwal'])) {
    $arrayHarian = $rawJadwal['jadwal']['a-harian'];
    $arrayHarian = array_map("unserialize", array_unique(array_map("serialize", $arrayHarian)));
    $apiJadwal['jadwal']['a-harian'] = $arrayHarian;
}

if (array_key_exists('b-mingguan', $rawJadwal['jadwal'])) {
    $arrayMingguan = $rawJadwal['jadwal']['b-mingguan'];
    $arrayMingguan = array_map("unserialize", array_unique(array_map("serialize", $arrayMingguan)));
    $apiJadwal['jadwal']['b-mingguan'] = $arrayMingguan;
}

if (array_key_exists('c-bulanan', $rawJadwal['jadwal'])) {
    $arrayBulanan = $rawJadwal['jadwal']['c-bulanan'];
    $arrayBulanan = array_map("unserialize", array_unique(array_map("serialize", $arrayBulanan)));
    $apiJadwal['jadwal']['c-bulanan'] = $arrayBulanan;
}

$userAdmin = array();
if (array_key_exists('userAdmin', $rawJadwal)) {
    $userAdmin = $rawJadwal['userAdmin'];
}

$apiJadwal['mesinKehadiran'] = $rawJadwal['mesinKehadiran'];

$kepulangan = $apiJadwal['jadwal'];
$mesinKehadiran = $apiJadwal['mesinKehadiran'];
$keyJadwal = 'c-bulanan';
$hariSekarang = date('N', time());
$tanggalSekarang = date('j', time());

if ($hariSekarang == 7) exit;

$logHandle = fopen(LOG_BULANAN, 'a+');

$ekstensiLimit = ".limit";
foreach (glob(LOG_DIR.'*'.$ekstensiLimit) as $filename) {
    if (strpos($filename, date('Y-m-d')) === false) {
        @unlink($filename);
    }
}

if (array_key_exists($keyJadwal, $kepulangan)) {
    foreach ($kepulangan[$keyJadwal] as $jadwal) {

        if ($jadwal['permulaan'] === false && $jadwal['hingga_jam'] != '' && $jadwal['tanggal'] == $tanggalSekarang) {
            $dari = strtotime(date('Y-m-d') . " {$jadwal['dari_jam']}");
            $hingga = strtotime(date('Y-m-d') . " {$jadwal['hingga_jam']}");
            $timestampSekarang = time();
            $selisihDenganAwal = $timestampSekarang - $dari;
            $selisihDenganAkhir = $timestampSekarang - $hingga;

            if ($selisihDenganAwal >= 0 && $selisihDenganAkhir <= BEDA_WAKTU_PULANG_MAKS) {
                foreach ($mesinKehadiran as $mesin) {
                    if ($mesin['aktif'] === true && $mesin['alamat_ip'] != '') {

                        fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Mengambil data dari mesin {$mesin['alamat_ip']} untuk jadwal tanggal ke {$jadwal['tanggal']} jam {$jadwal['dari_jam']} - {$jadwal['hingga_jam']}\n");

                        $options = array(
                            'ip' => $mesin['alamat_ip'],
                            'com_key' => $mesin['commkey'],
                        );

                        $tadFactory = new TADPHP\TADFactory($options);
                        $tad = $tadFactory->get_instance();

                        if ($tad !== null) {
                            try {
                                $tad->disable();
                                sleep(1);

                                $koneksiMesin = fsockopen($mesin['alamat_ip'], "80", $errno, $errstr, KONEKSI_FP_TIMEOUT);
                                $outputBuffer = '';

                                if ($koneksiMesin) {
                                    $counter  = 0;
                                    $ambilLog = true;
                                    $error400 = false;

                                    while ($ambilLog === true) {
                                        $counter ++;
                                        $outputBuffer = '';

                                        $soapRequest = "<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">{$mesin['commkey']}</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
                                        $newline     = "\r\n";
                                        fputs($koneksiMesin, "POST /iWsService HTTP/1.0" . $newline);
                                        fputs($koneksiMesin, "Content-Type: text/xml" . $newline);
                                        fputs($koneksiMesin, "Content-Length: " . strlen($soapRequest) . $newline . $newline);
                                        fputs($koneksiMesin, $soapRequest . $newline);

                                        while ($response = fgets($koneksiMesin, 1024)) {
                                            $outputBuffer .= $response;
                                        }

                                        if (stristr($outputBuffer, "400 Page not found") !== false || trim($outputBuffer) == '') {
                                            $ambilLog = true;
                                            $error400 = true;
                                            sleep(5);
                                        } else {
                                            $ambilLog = false;
                                            $error400 = false;
                                            break;
                                        }

                                        if ($counter >= 3 && $ambilLog === true) {
                                            $ambilLog = false;
                                        }
                                    }

                                    if ($error400) {
                                        fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: PERINGATAN! Mesin kehadiran {$mesin['alamat_ip']} error\n");
                                        fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Berusaha menyala-ulangkan mesin kehadiran {$mesin['alamat_ip']}\n");

                                        $tad->restart();

                                        $jumlahSmsHarian    = 0;
                                        $logJumlahSmsHarian = LOG_DIR . $mesin['alamat_ip'] . date('Y-m-d') . $ekstensiLimit;
                                        if (file_exists($logJumlahSmsHarian)) {
                                            $jumlahSmsHarian = intval(file_get_contents($logJumlahSmsHarian));
                                        }

                                        if (count($userAdmin) > 0 && $mesin['kirim_sms_peringatan'] === true && $jumlahSmsHarian < $mesin['maks_sms_harian']) {
                                            $pesan = "PERINGATAN!! Mesin kehadiran {$mesin['alamat_ip']} error, tak bisa diambil log kehadiran secara otomatis. Segera periksa.";
                                            $ch    = curl_init();
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                                            foreach ($userAdmin as $user) {
                                                if (array_key_exists('nomor_ponsel', $user)) {
                                                    $urlKirimPesan = SMS_PROVIDER;
                                                    $urlKirimPesan = str_replace("%nomor%", $user['nomor_ponsel'], $urlKirimPesan);
                                                    $urlKirimPesan = str_replace("%pesan%", urlencode($pesan), $urlKirimPesan);

                                                    curl_setopt($ch, CURLOPT_URL, $urlKirimPesan);
                                                    curl_exec($ch);

                                                    $jumlahHandle = fopen($logJumlahSmsHarian, 'w');
                                                    fwrite($jumlahHandle, ++ $jumlahSmsHarian);
                                                }
                                            }
                                            curl_close($ch);
                                        }
                                    } else {
                                        $filename      = $mesin['alamat_ip'] . '_' . time() . '.gz';
                                        $localFilePath = DATA_DIR . $filename;
                                        $gzResource    = gzopen($localFilePath, 'w9');
                                        gzwrite($gzResource, $outputBuffer);
                                        gzclose($gzResource);

                                        $koneksiFtp = ftp_connect(FTP_SERVER);
                                        $loginFtp   = ftp_login($koneksiFtp, FTP_USERNAME, FTP_PASSWORD);

                                        ftp_pasv($koneksiFtp, true);

                                        if ($koneksiFtp) {
                                            if ( ! ftp_chdir($koneksiFtp, LOG_DIR_BULANAN_REMOTE . date('Y-m-d'))) {
                                                ftp_mkdir($koneksiFtp, LOG_DIR_BULANAN_REMOTE . date('Y-m-d'));
                                            }
                                            if ( ! ftp_chdir($koneksiFtp, LOG_DIR_BULANAN_REMOTE . date('Y-m-d') . DIRECTORY_SEPARATOR . LOG_DIR_PULANG)) {
                                                ftp_mkdir($koneksiFtp, LOG_DIR_BULANAN_REMOTE . date('Y-m-d') . DIRECTORY_SEPARATOR . LOG_DIR_PULANG);
                                            }
                                            if (ftp_put($koneksiFtp, LOG_DIR_BULANAN_REMOTE . date('Y-m-d') . DIRECTORY_SEPARATOR . LOG_DIR_PULANG . $filename, $localFilePath, FTP_BINARY)) {
                                                @unlink($localFilePath);
                                            }
                                        } else {
                                            fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Tak bisa terhubung ke " . FTP_SERVER . "\n");
                                        }
                                    }
                                } else {
                                    fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Koneksi ke mesin kehadiran {$mesin['alamat_ip']} dengan commkey {$mesin['commkey']} gagal dilakukan\n");
                                    fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: $errstr ($errno)\n");
                                }

                                $tad->enable();
                            } catch (TADPHP\Exceptions\ConnectionError $exception) {
                                $pesan[] = "Mesin kehadiran {$mesin['alamat_ip']} tidak dapat dijangkau.";
                            }
                        }
                    } else {
                        if (MODE_DEBUG === 1) fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Mesin non-aktif {$mesin['alamat_ip']} atau tanpa alamat ip diabaikan\n");
                    }
                }
            } else {
                if (MODE_DEBUG === 1) fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Jadwal tanggal ke {$jadwal['tanggal']} jam {$jadwal['dari_jam']} - {$jadwal['hingga_jam']} yang berbeda > " . BEDA_WAKTU_PULANG_MAKS . " detik diabaikan\n");
                continue;
            }
        } else {
            if (MODE_DEBUG === 1) fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Jadwal tanggal ke {$jadwal['tanggal']} jam {$jadwal['dari_jam']} - " . ($jadwal['hingga_jam'] != '' ? $jadwal['hingga_jam'] : '--') . " diabaikan\n");
        }
    }
} else {
    if (MODE_DEBUG === 1) fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: $keyJadwal tidak ada dalam jadwal\n");
}

fclose($logHandle);
