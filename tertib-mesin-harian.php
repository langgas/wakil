<?php
/*
 * (c) 2014 Sisdik, <info@sisdik.com>
 */

require_once 'konfigurasi.php';

require_once 'lib/TADFactory.php';
require_once 'lib/TAD.php';
require_once 'lib/TADResponse.php';
require_once 'lib/Providers/TADSoap.php';
require_once 'lib/Providers/TADZKLib.php';
require_once 'lib/Exceptions/ConnectionError.php';
require_once 'lib/Exceptions/FilterArgumentError.php';
require_once 'lib/Exceptions/UnrecognizedArgument.php';
require_once 'lib/Exceptions/UnrecognizedCommand.php';

$jsonString = file_get_contents(JADWAL_DIR . BERKAS_KEHADIRAN);
$rawJadwal = json_decode($jsonString, true);
$apiJadwal = array();

if (array_key_exists('mesinKehadiran', $rawJadwal)) {
    $arrayMesinKehadiran = $rawJadwal['mesinKehadiran'];
    $arrayMesinKehadiran = array_map("unserialize", array_unique(array_map("serialize", $arrayMesinKehadiran)));
    $apiJadwal['mesinKehadiran'] = $arrayMesinKehadiran;
} else {
    exit;
}

$userAdmin = array();
if (array_key_exists('userAdmin', $rawJadwal)) {
    $userAdmin = $rawJadwal['userAdmin'];
}

$waktuSekarang = time();
$mesinKehadiran = $apiJadwal['mesinKehadiran'];

$hariSekarang = date('N', time()) - 1;
if ($hariSekarang == 6) exit;

$logHandle = fopen(LOG_TERTIB_HARIAN, 'a+');

$ekstensiLimit = ".limit";
foreach (glob(LOG_DIR.'*'.$ekstensiLimit) as $filename) {
    if (strpos($filename, date('Y-m-d')) === false) {
        unlink($filename);
    }
}

foreach ($mesinKehadiran as $mesin) {
    if ($mesin['aktif'] === true && $mesin['alamat_ip'] != '' && $mesin['waktu_tertib_harian'] != '') {
        $hingga = strtotime(date('Y-m-d') . " {$mesin['waktu_tertib_harian']}");
        $bedaWaktu = $waktuSekarang - $hingga;

        if ($bedaWaktu >= 0 && $bedaWaktu <= BEDA_WAKTU_ATUR_MAKS) {
            $options = array(
                'ip' => $mesin['alamat_ip'],
                'com_key' => $mesin['commkey'],
            );

            $tadFactory = new TADPHP\TADFactory($options);
            $tad = $tadFactory->get_instance();

            if ($tad !== null) {
                try {
                    fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Menghapus log kehadiran mesin kehadiran {$mesin['alamat_ip']}\n");
                    $tad->delete_data(array('value' => 3));

                    fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Menyelaraskan waktu mesin kehadiran {$mesin['alamat_ip']}\n");
                    $date = new \DateTime("now");
                    $tad->set_date();

                    sleep(1);

                    $pesan[] = "Berhasil menertibkan mesin kehadiran {$mesin['alamat_ip']} (hapus log, selaras waktu).";
                } catch (TADPHP\Exceptions\ConnectionError $exception) {
                    $pesan[] = "Mesin kehadiran {$mesin['alamat_ip']} tidak dapat dijangkau.";
                }
            } else {
                fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Tak bisa terhubung ke mesin kehadiran {$mesin['alamat_ip']} untuk menyelaraskan waktunya dan menghapus lognya\n");

                $jumlahSmsHarian = 0;
                $logJumlahSmsHarian = LOG_DIR.$mesin['alamat_ip'].date('Y-m-d').$ekstensiLimit;
                if (file_exists($logJumlahSmsHarian)) {
                    $jumlahSmsHarian = intval(file_get_contents($logJumlahSmsHarian));
                }

                if (count($userAdmin) > 0 && $mesin['kirim_sms_peringatan'] === true && $jumlahSmsHarian < $mesin['maks_sms_harian']) {
                    $pesan = "PERINGATAN!! Mesin kehadiran {$mesin['alamat_ip']} tak bisa ditertibkan (hapus log->sinkron waktu). Segera periksa.";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                    foreach ($userAdmin as $user) {
                        if (array_key_exists('nomor_ponsel', $user)) {
                            $urlKirimPesan = SMS_PROVIDER;
                            $urlKirimPesan = str_replace("%nomor%", $user['nomor_ponsel'], $urlKirimPesan);
                            $urlKirimPesan = str_replace("%pesan%", urlencode($pesan), $urlKirimPesan);

                            curl_setopt($ch, CURLOPT_URL, $urlKirimPesan);
                            curl_exec($ch);

                            $jumlahHandle = fopen($logJumlahSmsHarian, 'w');
                            fwrite($jumlahHandle, ++$jumlahSmsHarian);
                        }
                    }
                    curl_close($ch);
                }
            }
        }
    }
}

fclose($logHandle);
