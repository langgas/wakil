<?php
/*
 * (c) 2016 Sisdik, <info@sisdik.com>
 */

require_once 'konfigurasi.php';

require_once 'lib/TADFactory.php';
require_once 'lib/TAD.php';
require_once 'lib/TADResponse.php';
require_once 'lib/Providers/TADSoap.php';
require_once 'lib/Providers/TADZKLib.php';
require_once 'lib/Exceptions/ConnectionError.php';
require_once 'lib/Exceptions/FilterArgumentError.php';
require_once 'lib/Exceptions/UnrecognizedArgument.php';
require_once 'lib/Exceptions/UnrecognizedCommand.php';

$jsonString = file_get_contents(JADWAL_DIR . BERKAS_KEHADIRAN);
$rawJadwal = json_decode($jsonString, true);
$apiJadwal = array();

if (array_key_exists('mesinKehadiran', $rawJadwal)) {
    $arrayMesinKehadiran = $rawJadwal['mesinKehadiran'];
    $arrayMesinKehadiran = array_map("unserialize", array_unique(array_map("serialize", $arrayMesinKehadiran)));
    $apiJadwal['mesinKehadiran'] = $arrayMesinKehadiran;
} else {
    exit;
}

$mesinKehadiran = $apiJadwal['mesinKehadiran'];

$logHandle = fopen(LOG_TERTIB_MANUAL, 'a+');
$pesan = array();

foreach ($mesinKehadiran as $mesin) {
    if ($mesin['aktif'] === true && $mesin['alamat_ip'] != '') {
        fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Menertibkan manual mesin {$mesin['alamat_ip']}\n");

        $options = array(
            'ip' => $mesin['alamat_ip'],
            'com_key' => $mesin['commkey'],
        );

        $tadFactory = new TADPHP\TADFactory($options);
        $tad = $tadFactory->get_instance();

        if ($tad !== null) {
            try {
                fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Menghapus log kehadiran mesin kehadiran {$mesin['alamat_ip']}\n");
                $tad->delete_data(array('value' => 3));

                fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Menyelaraskan waktu mesin kehadiran {$mesin['alamat_ip']}\n");
                $date = new \DateTime("now");
                $tad->set_date();

                sleep(1);

                $pesan[] = "Berhasil menertibkan mesin kehadiran {$mesin['alamat_ip']} (hapus log, selaras waktu).";
            } catch (TADPHP\Exceptions\ConnectionError $exception) {
                $pesan[] = "Mesin kehadiran {$mesin['alamat_ip']} tidak dapat dijangkau.";
            }
        } else {
            fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Tak bisa terhubung ke mesin kehadiran {$mesin['alamat_ip']} untuk menertibkannya (hapus log, selaras waktu)\n");
            $pesan[] = "Tak bisa terhubung ke mesin kehadiran {$mesin['alamat_ip']} untuk menertibkannya (hapus log, selaras waktu). Segera periksa.";
        }
    }
}

fclose($logHandle);

$returnValue['pesan'] = $pesan;

print json_encode($returnValue);
