<?php
/*
 * (c) 2014 Sisdik, <info@sisdik.com>
 */

require_once 'konfigurasi.php';

$berkasKepulangan = JADWAL_DIR . BERKAS_KEPULANGAN;

$protocol = 'https';
if (defined('NON_SSL')) {
    if (NON_SSL === true) {
        $protocol = 'http';
    }
}

$arrContextOptions=array(
    "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ),
);

if (defined('DEV_BOX')) {
    if (DEV_BOX != '') {
        $jadwalKepulangan = file_get_contents($protocol.'://'.FTP_SERVER.'/'.DEV_BOX.'/_api/jadwal-kepulangan/'.API_TOKEN, false, stream_context_create($arrContextOptions));
    }
} else {
    $jadwalKepulangan = file_get_contents($protocol.'://'.FTP_SERVER.'/_api/jadwal-kepulangan/'.API_TOKEN, false, stream_context_create($arrContextOptions));
}

$jadwalHandle = fopen($berkasKepulangan, 'w');
$logHandle = fopen(LOG_MANUAL, 'a+');

fwrite($jadwalHandle, $jadwalKepulangan);
fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Memperbarui jadwal kepulangan\n");

fclose($jadwalHandle);
fclose($logHandle);

$returnValue['pesan'][] = "Informasi jadwal dan mesin kehadiran di mesin wakil-sisdik berhasil diperbarui.";

print json_encode($returnValue);
