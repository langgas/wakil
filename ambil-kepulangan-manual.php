<?php
/*
 * (c) 2014 Sisdik, <info@sisdik.com>
 */

require_once 'konfigurasi.php';

require_once 'lib/TADFactory.php';
require_once 'lib/TAD.php';
require_once 'lib/TADResponse.php';
require_once 'lib/Providers/TADSoap.php';
require_once 'lib/Providers/TADZKLib.php';
require_once 'lib/Exceptions/ConnectionError.php';
require_once 'lib/Exceptions/FilterArgumentError.php';
require_once 'lib/Exceptions/UnrecognizedArgument.php';
require_once 'lib/Exceptions/UnrecognizedCommand.php';

$jsonString = file_get_contents(JADWAL_DIR . BERKAS_KEPULANGAN);
$rawJadwal = json_decode($jsonString, true);
$mesinKehadiran = $rawJadwal['mesinKehadiran'];

$logHandle = fopen(LOG_MANUAL, 'a+');
$adaLogTerkirim = false;
$pesan = array();

foreach ($mesinKehadiran as $mesin) {
    if ($mesin['aktif'] === true && $mesin['alamat_ip'] != '') {
        fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Mengambil data dari mesin {$mesin['alamat_ip']}\n");

        $options = array(
            'ip' => $mesin['alamat_ip'],
            'com_key' => $mesin['commkey'],
        );

        $tadFactory = new TADPHP\TADFactory($options);
        $tad = $tadFactory->get_instance();

        if ($tad !== null) {
            try {
                $tad->disable();
                sleep(1);

                $koneksiMesin = fsockopen($mesin['alamat_ip'], "80", $errno, $errstr, KONEKSI_FP_TIMEOUT);
                $outputBuffer = '';

                if ($koneksiMesin) {
                    $soapRequest = "<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">" . $mesin['commkey'] . "</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
                    $newline     = "\r\n";
                    fputs($koneksiMesin, "POST /iWsService HTTP/1.0" . $newline);
                    fputs($koneksiMesin, "Content-Type: text/xml" . $newline);
                    fputs($koneksiMesin, "Content-Length: " . strlen($soapRequest) . $newline . $newline);
                    fputs($koneksiMesin, $soapRequest . $newline);

                    while ($response = fgets($koneksiMesin, 1024)) {
                        $outputBuffer .= $response;
                    }

                    if (stristr($outputBuffer, "400 Page not found") !== false || trim($outputBuffer) == '') {
                        $pesan[] = "Mesin kehadiran {$mesin['alamat_ip']} error";
                        fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: PERINGATAN! Mesin kehadiran {$mesin['alamat_ip']} error\n");
                        fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Berusaha menyala-ulangkan mesin kehadiran {$mesin['alamat_ip']}\n");

                        $tad->restart();
                    } else {
                        $tad->enable();
                    }

                    $filename      = $mesin['alamat_ip'] . '_' . time() . '.gz';
                    $localFilePath = DATA_DIR . $filename;
                    $gzResource    = gzopen($localFilePath, 'w9');
                    gzwrite($gzResource, $outputBuffer);
                    gzclose($gzResource);

                    $koneksiFtp = ftp_connect(FTP_SERVER);
                    $loginFtp   = ftp_login($koneksiFtp, FTP_USERNAME, FTP_PASSWORD);

                    ftp_pasv($koneksiFtp, true);

                    if ($koneksiFtp) {
                        if ( ! ftp_chdir($koneksiFtp, LOG_DIR_MANUAL_REMOTE . date('Y-m-d'))) {
                            ftp_mkdir($koneksiFtp, LOG_DIR_MANUAL_REMOTE . date('Y-m-d'));
                        }
                        if ( ! ftp_chdir($koneksiFtp, LOG_DIR_MANUAL_REMOTE . date('Y-m-d') . DIRECTORY_SEPARATOR . LOG_DIR_PULANG)) {
                            ftp_mkdir($koneksiFtp, LOG_DIR_MANUAL_REMOTE . date('Y-m-d') . DIRECTORY_SEPARATOR . LOG_DIR_PULANG);
                        }
                        if (ftp_put($koneksiFtp, LOG_DIR_MANUAL_REMOTE . date('Y-m-d') . DIRECTORY_SEPARATOR . LOG_DIR_PULANG . $filename, $localFilePath, FTP_BINARY)) {
                            $pesan[]        = "Berhasil mengirim log kepulangan dari mesin kehadiran {$mesin['alamat_ip']} ke server";
                            $adaLogTerkirim = true;
                            @unlink($localFilePath);
                        }
                    } else {
                        $pesan[] = "Tak bisa terhubung ke server untuk mengirim log kepulangan dari {$mesin['alamat_ip']}";
                        fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Tak bisa terhubung ke " . FTP_SERVER . "\n");
                    }
                } else {
                    $pesan[] = "Koneksi ke mesin kehadiran {$mesin['alamat_ip']} dengan commkey {$mesin['commkey']} gagal dilakukan";
                    fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Koneksi ke mesin kehadiran {$mesin['alamat_ip']} dengan commkey {$mesin['commkey']} gagal dilakukan\n");
                    fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: $errstr ($errno)\n");
                }

                $tad->enable();
            } catch (TADPHP\Exceptions\ConnectionError $exception) {
                $pesan[] = "Mesin kehadiran {$mesin['alamat_ip']} tidak dapat dijangkau.";
            }
        }
    } else {
        if (MODE_DEBUG === 1) fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Mesin non-aktif {$mesin['alamat_ip']} atau tanpa alamat ip diabaikan\n");
    }
}

fclose($logHandle);

$returnValue['adaLogTerkirim'] = $adaLogTerkirim;
$returnValue['pesan'] = $pesan;

print json_encode($returnValue);
