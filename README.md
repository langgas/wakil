# WAKIL SISDIK #

Wakil sisdik berguna untuk mengambil data dari mesin fingerprint lalu mengirimkannya ke server sisdik

Untuk pertama kali pembekalan wakil sisdik, jalankan perintah console berikut di wakil sisdik:

```
#!bash
$ wget https://bitbucket.org/langgas/wakil/raw/master/provisioning/provision-raspbian.sh --no-check-certificate -O provision-raspbian.sh
$ bash provision-raspbian.sh <id-sekolah> <password-ftp> <token-api> <ftp/web-server, default www.sisdik.com>
```

Untuk memperbarui jadwal dan/atau script, jalankan perintah berikut:

```
#!bash
$ wget https://bitbucket.org/langgas/wakil/raw/master/provisioning/provision-raspbian.sh --no-check-certificate -O provision-raspbian.sh && bash provision-raspbian.sh
```
