<?php
/*
 * (c) 2014 Sisdik, <info@sisdik.com>
 */

require_once 'konfigurasi.php';

$berkasKehadiran = JADWAL_DIR . BERKAS_KEHADIRAN;

$protocol = 'https';
if (defined('NON_SSL')) {
    if (NON_SSL === true) {
        $protocol = 'http';
    }
}

$arrContextOptions=array(
    "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ),
);

if (defined('DEV_BOX')) {
    if (DEV_BOX != '') {
        $jadwalKehadiran = file_get_contents($protocol.'://'.FTP_SERVER.'/'.DEV_BOX.'/_api/jadwal-kehadiran/'.API_TOKEN, false, stream_context_create($arrContextOptions));
    }
} else {
    $jadwalKehadiran = file_get_contents($protocol.'://'.FTP_SERVER.'/_api/jadwal-kehadiran/'.API_TOKEN, false, stream_context_create($arrContextOptions));
}

$jadwalHandle = fopen($berkasKehadiran, 'w');
$logHandle = fopen(LOG_MANUAL, 'a+');

fwrite($jadwalHandle, $jadwalKehadiran);
fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Memperbarui jadwal kehadiran\n");

fclose($jadwalHandle);
fclose($logHandle);

$returnValue['pesan'][] = "Informasi jadwal dan mesin kehadiran di mesin wakil-sisdik berhasil diperbarui.";

print json_encode($returnValue);
