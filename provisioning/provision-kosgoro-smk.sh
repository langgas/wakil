#!/bin/bash

cd /home/wakil/smk

if [[ $# -lt 3 ]] ; then
    if [ -d "wakil-sisdik" ] ; then
        echo "PERINGATAN: Pembekal akan mencoba menggunakan berkas konfigurasi yang sama dengan sebelumnya"
    else
        echo "Penggunaan: bash $0 <FTP_USERNAME> <FTP_PASSWORD> <API_TOKEN> <FTP/WEB_SERVER_IP>"
        echo "ERROR: Pertama kali pembekal dijalankan harus menyediakan informasi parameter yang diperlukan!"
        exit 0
    fi
fi

echo "[konfigurasi script wakil-sisdik]"

wget https://bitbucket.org/langgas/wakil/get/master.tar.gz --no-check-certificate
original_directory=$(tar -tf master.tar.gz | grep langgas-wakil -m 1)
target_directory=${original_directory%?}
tar -xvzf master.tar.gz

if [ -d "wakil-sisdik" ] ; then
    cp wakil-sisdik/konfigurasi.php $target_directory/konfigurasi.php
    cp wakil-sisdik/jadwal/* $target_directory/jadwal/
    cp wakil-sisdik/data/* $target_directory/data/
    cp wakil-sisdik/log/* $target_directory/log/

    tmpstring=$(grep FTP_USERNAME $target_directory/konfigurasi.php)
    tmpstring=${tmpstring/*FTP_USERNAME\', \'/}
    tmpstring=${tmpstring/\');/}
    FTP_USERNAME=$tmpstring
    if [ ! -z "$1" ] ; then
        sed -i "s/$FTP_USERNAME/$1/g" $target_directory/konfigurasi.php
        FTP_USERNAME=$1
    fi

    tmpstring=$(grep FTP_PASSWORD $target_directory/konfigurasi.php)
    tmpstring=${tmpstring/*FTP_PASSWORD\', \'/}
    tmpstring=${tmpstring/\');/}
    FTP_PASSWORD=$tmpstring
    if [ ! -z "$2" ] ; then
        sed -i "s/$FTP_PASSWORD/$2/g" $target_directory/konfigurasi.php
        FTP_PASSWORD=$2
    fi

    tmpstring=$(grep API_TOKEN $target_directory/konfigurasi.php)
    tmpstring=${tmpstring/*API_TOKEN\', \'/}
    tmpstring=${tmpstring/\');/}
    API_TOKEN=$tmpstring
    if [ ! -z "$3" ] ; then
        sed -i "s/$API_TOKEN/$3/g" $target_directory/konfigurasi.php
        API_TOKEN=$3
    fi

    tmpstring=$(grep FTP_SERVER $target_directory/konfigurasi.php)
    tmpstring=${tmpstring/*FTP_SERVER\', \'/}
    tmpstring=${tmpstring/\');/}
    FTP_SERVER=$tmpstring
    if [ ! -z "$4" ] ; then
        sed -i "s/$FTP_SERVER/$4/g" $target_directory/konfigurasi.php
        FTP_SERVER=$4
    fi

    rm -fr wakil-sisdik
else
    cp $target_directory/konfigurasi.dist.php $target_directory/konfigurasi.php

    if [ ! -z "$1" ] ; then
        sed -i "s/ftpusername/$1/g" $target_directory/konfigurasi.php
        FTP_USERNAME=$1
    fi

    if [ ! -z "$2" ] ; then
        sed -i "s/ftppassword/$2/g" $target_directory/konfigurasi.php
        FTP_PASSWORD=$2
    fi

    if [ ! -z "$3" ] ; then
        sed -i "s/apitoken/$3/g" $target_directory/konfigurasi.php
        API_TOKEN=$3
    fi

    FTP_SERVER="www.sisdik.com"
    if [ ! -z "$4" ] ; then
        sed -i "s/www\.sisdik\.com/$4/g" $target_directory/konfigurasi.php
        FTP_SERVER=$4
    fi
fi

curl http://$FTP_SERVER/_api/jadwal-kehadiran/$API_TOKEN > $target_directory/jadwal/kehadiran.json
curl http://$FTP_SERVER/_api/jadwal-kepulangan/$API_TOKEN > $target_directory/jadwal/kepulangan.json
mv $target_directory wakil-sisdik
chmod 777 -R wakil-sisdik/data/ wakil-sisdik/log/ wakil-sisdik/jadwal/

echo "[mengaktifkan cron kehadiran dan kepulangan]"
crontab /home/wakil/smk/wakil-sisdik/provisioning/cron/commands-kosgoro
crontab -l

rm -f master.tar.gz provision-kosgoro-smk.sh
echo "[selesai]"
